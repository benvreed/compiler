package test;

import org.junit.Test;
import me.codeblooded.lexical.*;
import static org.junit.Assert.*;

public class FileTokenizerTest {

    /**
     * Tests that peeking over the next token does not consume it.
     * @throws Exception
     */
    @Test public void testNextTokenDoesNotConsumeToken() throws Exception {
        FileTokenizer fileTokenizer = new FileTokenizer("/Users/ben/dev/Compiler/src/test/stage/program.txt");
        Token token = fileTokenizer.nextToken();
        Token token2 = fileTokenizer.nextToken();

        String token1Contents = token.contents;
        String token2Contents = token2.contents;

        assertEquals(token1Contents.equals(token2Contents), true);
    }

    /**
     * Tests that getting a token works.
     * @throws Exception
     */
    @Test public void testGetTokenReturnsToken() throws Exception {
        FileTokenizer fileTokenizer = new FileTokenizer("/Users/ben/dev/Compiler/src/test/stage/program.txt");
        Token token = fileTokenizer.getToken();

        // assert getting token works
        assertEquals(token.contents.equals("program"), true);
    }

    /**
     * Tests that getting a token consumes a token.
     * @throws Exception
     */
    @Test public void testGetTokenConsumesToken() throws Exception {
        FileTokenizer fileTokenizer = new FileTokenizer("/Users/ben/dev/Compiler/src/test/stage/program.txt");
        Token token = fileTokenizer.getToken();
        Token token2 = fileTokenizer.getToken();

        String token1Contents = token.contents;
        String token2Contents = token2.contents;

        // assert token is different
        assertEquals(token1Contents.equals(token2Contents), false);
    }

    /**
     * Tests that a unique id is assigned.
     * @throws Exception
     */
    @Test public void testTokenAssignedUniqueID() throws Exception {
        FileTokenizer fileTokenizer = new FileTokenizer("/Users/ben/dev/Compiler/src/test/stage/program.txt");
        Token token = fileTokenizer.getToken();
        Token token2 = fileTokenizer.getToken();

        int token1ID = token.index;
        int token2ID = token2.index;

        assertNotEquals(token1ID, token2ID);
    }
}