package me.codeblooded;

import me.codeblooded.lexical.FileTokenizer;
import me.codeblooded.lexical.Token;
import me.codeblooded.lexical.TokenIdentifier;
import me.codeblooded.syntax.KeywordIdentifier;
import me.codeblooded.syntax.SyntaxAnalyzer;
import me.codeblooded.notificationCenter.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileTokenizer fileTokenizer = new FileTokenizer("/Users/ben/dev/Compiler/src/test/stage/program.txt");
            Token token;
            System.out.println("fileTokenizer connected.");
//            for (int q=0; q < 25;  q++) {
////                System.out.print("line "+q+" ");
//                System.out.println(q + " " +fileTokenizer.getToken().contents);
////                fileTokenizer.getToken();
////                System.out.println();
//            }

            NoteCenter.activeLogLevel = LogLevel.VERBOSE;

            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(fileTokenizer);
            syntaxAnalyzer.P();



            fileTokenizer.closeOut();
        }
        catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }
}
