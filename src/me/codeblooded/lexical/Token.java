package me.codeblooded.lexical;

/**
 * A Token is the representation of grouped objects with varying importance during
 * lexical analysis.
 */
public class Token {

    /**
     * The contents of the Token.
     */
    public String contents;

    /**
     * A unique number assigned to the token.
     */
    public int index;

    /**
     * An identifier for the type of token.
     */
    public TokenIdentifier identifier;

    /**
     * Internal string builder for populating a token's contents.
     * Not to be used for public access.
     */
    private StringBuilder stringBuilder;

    public Token() {
        stringBuilder = new StringBuilder();
    }

    /**
     * A shortcut for adding a char to the contents of a Token.
     * @param field The char appended.
     */
    public void add(char field) {
        stringBuilder.append(field);
        this.contents = stringBuilder.toString();
    }

    /**
     * Makes a copy of the token.
     */
    public Token copy() {
        Token token = new Token();
        token.index = index;
        token.contents = contents;
        token.identifier = identifier;
        return token;
    }

}
