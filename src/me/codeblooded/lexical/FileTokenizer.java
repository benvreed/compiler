package me.codeblooded.lexical;

import com.sun.jmx.remote.internal.ArrayQueue;
import me.codeblooded.syntax.KeywordIdentifier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * FileTokenizer is an abstract representation of a machine which tokenizes an input file.
 * It provides look-ahead (or peek) functionality.  It also does not read the entire file
 * into memory in order to tokenize it. Instead, it tracks the last position in the file
 * and builds up tokens lazily.
 */
public class FileTokenizer {

    private BufferedReader  bufferedReader;
    private FileReader      fileReader;
    private Token           collectable;
    private Token           archive;
    private boolean         consumed = false;
    private boolean         commentDetected = false;
    private int             tokenCount = 0;
    private int             line = 1;
    private KeywordIdentifier identifier;

    public FileTokenizer(String fileName) throws IOException {
        fileReader = new FileReader(fileName);
        bufferedReader = new BufferedReader(fileReader);
        archive = new Token();
        archive.contents = "";
        collectable = null;
        identifier = new KeywordIdentifier();
    }

    /**
     * nextToken() peeks at the next token.  It doesn't consume it; therefore, if getToken()
     * is called it will return what is peeked. This allows a look-ahead.
     * @return The next Token.
     * @see FileTokenizer#getToken()
     */
    public Token nextToken() throws IOException {
        if (collectable == null || consumed == true) {
            if (archive.contents != "") {
                consumed = false;
                collectable = new Token();
                collectable.contents = archive.contents;
                archive.contents = "";
//                System.out.println("token {"+collectable.contents+"} in archive.");
                identifier.identify(collectable);
                return collectable;
            }
            else {
                consumed = false;
                collectable = buildUp();
                while (collectable.contents == null || collectable.contents == "") {
                    collectable = buildUp();
                }
                if (collectable.contents.equals("/*")) {
                    // ignore comments

                    while (!collectable.contents.equals("*/")) {
                        getToken();
                    }
                    getToken();
                }
//                System.out.println("token {"+collectable.contents+"} in consumed.");
                identifier.identify(collectable);
                return collectable;
            }
        }
        else {
//            System.out.println("token {"+collectable.contents+"} in unconsumed.");
            return collectable;
        }
    }

    

    /**
     * getToken() grabs the next token and consumes it.  Therefore, it increments the place
     * in the file and will return the next available token.  This directly contrasts
     * nextToken()
     * @return The next Token
     * @see FileTokenizer#nextToken()
     */
    public Token getToken() throws IOException {
        consumed = true;
        return nextToken();
    }

    public void closeOut() throws IOException {
        bufferedReader.close();
        fileReader.close();
    }

    /**
     * Get the current line being tokenized.
     * @return An integer representing the line.
     */
    public int getLine() {
        return line;
    }

    /**
     * A private method which builds up the contents of a
     * token.  It uses a loop to check for delimiters.
     * @throws IOException
     */
    private Token buildUp() throws IOException {
        Token build = new Token();
        char z = (char)bufferedReader.read();

        if (z == '(') {
            build.add(z);
            build.index = ++tokenCount;
            return build;
        }
        else if (z == ')') {
            build.add(z);
            build.index = ++tokenCount;
            return build;
        }
        else if (z == ';') {
            build.add(z);
            build.index = ++tokenCount;
            return build;
        }

        while (!delimiterFound(z)) {
            build.add(z);
            z = (char)bufferedReader.read();
        }

        if (build.contents != null) {
            build.index = ++tokenCount;
        }

        ignoreComments(build);
//        System.out.println(build.contents);
        return build;
    }

    /**
     * Checks to see if a character is a delimiter.
     * @param z The character to perform the analysis on.
     * @return True if Delimiter and False otherwise.
     */
    private boolean delimiterFound(char z) {
        switch (z) {
            case ';':
                archive.contents = ";";
                return true;
            case ',':
                archive.contents = ",";
                return true;
            case '(':
                archive.contents = "(";
                return true;
            case ')':
                archive.contents = ")";
                return true;
            case ':':
                archive.contents = ":";
                return true;
            case ' ':
                return true;
            case '\n':
            case '\r':
                line++;
                return true;
            case '\t':
            case '\0':
                return true;
            default:
                return false;
        }
    }

    private void ignoreComments(Token token) throws IOException {
//        boolean endDetected = false;
//        if (token.contents != null) {
//            if(token.contents.equals("/*")) {
//                while (!token.contents.equals("*/")) {
//                    token = null;
//                    buildUp();
//                }
//                // check if closing comment
//                if (token.contents.equals("*/")) {
//                    // reset the token to the next important element.
//                    token = null;
//                    buildUp();
//                }
//                else  {
//                    System.out.println("(COMPILER)-> ERROR: EXPECTED ENDING COMMENT INDICATOR '*/'");
//                }
//            }
//        }
    }


}
