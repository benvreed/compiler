package me.codeblooded.lexical;

/**
 * Created by ben on 9/24/14.
 */
public enum TokenIdentifier {

    TokenIdentifierTypeKeyword,

    TokenIdentifierTypeConstant,

    TokenIdentifierTypeIdentifier
}
