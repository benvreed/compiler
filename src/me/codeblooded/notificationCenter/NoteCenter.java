package me.codeblooded.notificationCenter;

//import me.codeblooded.lexical.FileTokenizer;
//import me.codeblooded.lexical.LogLevel;

import java.time.LocalDateTime;

/**
 * Created by ben on 10/31/14.
 */
public class NoteCenter {

    public static LogLevel activeLogLevel;

    public static void error(String message, int line) {

        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println("(ERROR) [" + dateTime.toString() + "]: " + message + " (line " + line + ")");
    }

    public static void warn(String message) {

        if (activeLogLevel == LogLevel.WARN) {
            System.out.println("(WARNING) " + message + ".");
        }
    }

    public static void verbose(String message) {
        if (activeLogLevel == LogLevel.VERBOSE) {
            System.out.println("--(VERBOSE)  " + message + ".--");
        }
    }


}
