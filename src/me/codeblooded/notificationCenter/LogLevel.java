package me.codeblooded.notificationCenter;

/**
 * Created by ben on 9/24/14.
 */
public enum LogLevel {

    VERBOSE,

    WARN,

    ERROR
}
