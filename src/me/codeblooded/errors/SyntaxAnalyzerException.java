package me.codeblooded.errors;

import me.codeblooded.lexical.Token;
import me.codeblooded.lexical.TokenIdentifier;

/**
 * Created by ben on 9/27/14.
 */
public class SyntaxAnalyzerException extends Exception {

    private TokenIdentifier identifiedTokenIdentifier;
    private Token problematicToken;

    public SyntaxAnalyzerException(Token token) {
        super("Token {" + token.index + "|" + token.contents + "} could not be identified as a keyword, constant, or identifier.");
        problematicToken = token;
    }

}
