package me.codeblooded.syntax;

import me.codeblooded.errors.SyntaxAnalyzerException;
import me.codeblooded.lexical.*;
import me.codeblooded.notificationCenter.*;

import java.io.IOException;

/**
 * The SyntaxAnalyzer uses recursive descent in order to verify that a certain program
 * is part of the LL(1) grammar this compiler recognizes.  For reference, a copy
 * of the grammar is found here:
 *
 *      P -> program D begin S end
 *      D -> IL:D' | EPSILON
 *      D' -> AR D | integer D
 *      AR -> array AR'
 *      AR' -> (AR) | EPSILON
 *      IL -> ID IL | , ID IL | EPSILON
 *      ID -> id ID'
 *      ID' -> (SB) | EPSILON
 *      SB -> cons SB | id SB | , SB' | EPSILON
 *      SB' -> cons SB | id SB
 *      S -> do S S' | (IL) S'' | assign E to ID; S | EPSILON
 *      S' -> unless C; S | when C S'''
 *      S'' -> in; S | out; S
 *      S''' -> ; S | else S; S
 *      E -> id | cons | + E E | - E E | * E E | / E E
 *      C -> < E E | > E E | = E E | and C C | not C C
 */
public class SyntaxAnalyzer {
    /**
     * A reference to the file tokenizer provided by the constructor.
     */
    private FileTokenizer fileTokenizer;

    private KeywordIdentifier keywordIdentifier;

    /**
     * Constructor for the manager of syntax analysis.
     * @param fileTokenizer The current file's Tokenizer.
     */
    public SyntaxAnalyzer(FileTokenizer fileTokenizer) {
        this.fileTokenizer = fileTokenizer;
        this.keywordIdentifier = new KeywordIdentifier();
    }

    /**
     * A convenience method for peeking on the next token.
     * @return The Evaluation of fileTokenizer.nextToken()
     * @throws IOException
     * @see me.codeblooded.lexical.FileTokenizer#nextToken() nextToken() method description
     */
    private Token NT() throws IOException, SyntaxAnalyzerException {
        Token temp = fileTokenizer.nextToken();
        keywordIdentifier.identify(temp);
        return temp;
    }

    /**
     * A convenience method for getting and consuming the next token.
     * @return The Evaluation of fileTokenizer.getToken()
     * @throws IOException
     * @see me.codeblooded.lexical.FileTokenizer#getToken() getToken() method description
     */
    private Token GT() throws IOException, SyntaxAnalyzerException {
        Token temp = fileTokenizer.getToken();
        keywordIdentifier.identify(temp);
        return temp;
    }

    // Danger Zone:
    // Anything below this line is actually used to analyze the syntax of the program.
    // It strictly adheres to the LL(1) grammar.

    // P -> program D begin S end
    public boolean P() throws Exception {
        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within P()");
        if (NT().contents.equals("program")) {
            GT();

            NoteCenter.verbose("Just detected and consumed program");

            if (D()) {

                NoteCenter.verbose("D() returned successful");

                if (NT().contents.equals("begin")) {
                    GT();

                    NoteCenter.verbose("Just detected and consumed begin");

                    if (S()) {

                        NoteCenter.verbose("S() returned successfully");
                        if (NT().contents.equals("end.")) {
                            // GT();
                            NoteCenter.verbose("Just detected end ... program should be ok");
                            return true;
                        }
                        else {
                            NoteCenter.error("EXPECTED END OF PROGRAM INDICATOR 'end.' but got '" + NT().contents + "'", fileTokenizer.getLine());
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    NoteCenter.error("EXPECTED BEGINNING OF PROGRAM INDICATOR 'begin' but got '" + NT().contents + "'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {


            NoteCenter.error("EXPECTED BEGINNING OF PROGRAM INDICATOR 'program' but got '" + NT().contents + "'", fileTokenizer.getLine());

            return false;
        }
    }

    // D -> IL:D' | EPSILON                                         { id , ; begin }
    public boolean D() throws Exception {
        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within D()");
        String[] terminalSelectionSet = {",", ";", "begin"};
        String[] selectionSet2 = {",", ";"};
        if (isInSet(NT(), selectionSet2) || typeIsInSet(NT(), TokenIdentifier.TokenIdentifierTypeIdentifier)) {
            NoteCenter.verbose("D() found object in selection set, returning true");
            if (IL()) {
                NoteCenter.verbose("Successfully returned to D() from IL()");
                if (NT().contents.equals(":")) {
                    GT();
                    NoteCenter.verbose("D() successfully matched Terminal ':' and consumed it");
                    if (D1()) {
                        NoteCenter.verbose("D1() returned successfully; returning true");
                        return true;
                    } else {
                        NoteCenter.verbose("Problem detected in D1(); returning false");
                        return false;
                    }
                } else {
                    NoteCenter.error("Expected ':' but got '" + NT().contents + "'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (isInSet(NT(), terminalSelectionSet) || typeIsInSet(NT(), TokenIdentifier.TokenIdentifierTypeIdentifier)) {
            NoteCenter.verbose("D() found object in selection set, returning true");
            return true;
        }
        else {
            NoteCenter.verbose("Problem detected in D(); item not in selection set; returning false");
            return false;
        }
    }
    // D' -> AR D | integer D
    public boolean D1() throws Exception {

        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within D1()");
        if (AR()) {
            NoteCenter.verbose("Successfully returned from AR() to D1()");
            if (D()) {
                NoteCenter.verbose("D() successfully returned to D1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("D()->D1() detected a problem; returning false");
                return false;
            }
        }
        else if (NT().contents.equals("integer")) {
            GT();
            NoteCenter.verbose("Successfully matched terminal 'integer' and consumed it in D1()");
            if (D()) {
                NoteCenter.verbose("D() successfully returned to D1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("D()->D1() detected a problem; returning false");
                return false;
            }
        }
        else {
            NoteCenter.verbose("D1() detected a problem; returning false");
            return false;
        }
    }

    // AR -> array AR'
    public boolean AR() throws Exception {

        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within AR()");
        if (NT().contents.equals("array")) {
            GT();
            NoteCenter.verbose("AR() identified terminal 'array' and consumed it");
            if (AR1()) {
                NoteCenter.verbose("Successfully returned to AR() from AR1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in AR1()->AR(); returning false");
                return false;
            }
        }
        else if (NT().contents.equals("integer")) {
            return false;
        }
        else {
            NoteCenter.error("Expected 'array' but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // AR' -> (AR) | EPSILON                                        { ( id , : ) begin }
    public boolean AR1() throws Exception {

        String[] terminalSelectionSet = {"(", ",", ":", ")", "begin"};
        if (NT().contents.equals("(")) {
            GT();
            NoteCenter.verbose("AR1() successfully identified terminal '(' and consumed it");
            if (AR()) {
                NoteCenter.verbose("Returned Successfully from AR() to AR1()");
                if (NT().contents.equals(")")) {
                    GT();
                    return true;
                }
                else {
                    NoteCenter.error("Expected ')' but got '"+NT().contents+"'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                NoteCenter.verbose("Problem detected in AR() from AR1(); returning false");
                return false;
            }
        }
        else if (isInSet(NT(), terminalSelectionSet) || typeIsInSet(NT(), TokenIdentifier.TokenIdentifierTypeIdentifier)) {
            NoteCenter.verbose("AR1() successfully identified terminal/type in selection set; returning true");
            return true;
        }
        else {
            NoteCenter.error("Expected '(', ',', ':', ')', 'begin' or identifier but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // IL -> ID IL | , ID IL | EPSILON                              { id , : ) }
    public boolean IL() throws Exception {

        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within IL()");
        String[] terminalSelectionSet = { ",", ":", ")" };
        System.out.println("Print statement 1");
        if (ID()) {
            System.out.println("Print statement 2");
            NoteCenter.verbose("ID() just returned to IL() Successfully");
            if (IL()) {
                System.out.println("Print statement 3");
                NoteCenter.verbose("IL() just returned to IL() Successfully; returning true");
                return true;
            }
            else {
                NoteCenter.verbose("IL() detected a problem then returned to IL(); returning false");
                return false;
            }
        }
        else if (NT().contents.equals(",")) {
            GT();
            NoteCenter.verbose("D() successfully matched Terminal ',' and consumed it");
            if (ID()) {
                NoteCenter.verbose("ID() just returned to IL() Successfully");
                if (IL()) {
                    NoteCenter.verbose("IL() just returned to IL() Successfully; returning true");
                    return true;
                }
                else {
                    NoteCenter.verbose("IL() detected a problem then returned to IL(); returning false");
                    return false;
                }
            }
            else {
                NoteCenter.verbose("ID() detected a problem then returned to IL(); returning false");
                return false;
            }
        }
        else if (isInSet(NT(), terminalSelectionSet) || typeIsInSet(NT(), TokenIdentifier.TokenIdentifierTypeIdentifier)) {
            NoteCenter.verbose("Did match terminal or identifier in IL() selection set; returning true");
            System.out.println("Token = "+NT().contents);
            return true;
        }
        else {
            NoteCenter.error("Expected ',', ':', ')', or IDENTIFIER but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // ID -> id ID'
    public boolean ID() throws Exception {

        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within ID()");
        if (NT().identifier ==  TokenIdentifier.TokenIdentifierTypeIdentifier) {
            GT();
            NoteCenter.verbose("ID() found identifier and consumed it");
            if (ID1()) {
                NoteCenter.verbose("ID1() returned to ID() successfully; returning true");
                return true;
            }
            else {
                NoteCenter.verbose("ID1() detected an error; returning false");
                return false;
            }
        }
        else {
            NoteCenter.verbose("Identifier not found when expected in ID(); returning false");
            return false;
        }
    }

    // ID' -> (SB) | EPSILON                                        { ( id , ; : ) }
    public boolean ID1() throws Exception {
        NoteCenter.verbose("[SYNTAX-ANALYZER] Scope now within ID1()");
        String[] termSelectionSet = {"(", ",",";",":",")"};
        if (NT().contents.equals("(")) {
            GT();
            NoteCenter.verbose("ID1() successfully recognized '(' and consumed it");
            if (SB()) {
                NoteCenter.verbose("SB() successfully returned to ID1()");
                if (NT().contents.equals(")")) {
                    GT();
                    NoteCenter.verbose("ID1() successfully recognized ')' and consumed it");
                    return true;
                }
                else {
                    NoteCenter.error("Expected ')' but got '"+NT().contents+"'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                NoteCenter.verbose("Issue detected in SB()->ID1(); returning false");
                return false;
            }
        }
        else if (isInSet(NT(), termSelectionSet) || typeIsInSet(NT(), TokenIdentifier.TokenIdentifierTypeIdentifier)) {
            NoteCenter.verbose("ID1() did identify item in selection set; returning true");
            return true;
        }
        else {
            NoteCenter.error("Expected '(', ',', ';', ':', ')' or identifier but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // SB -> cons SB | id SB | , SB' | EPSILON                  { , ) cons id }
    public boolean SB() throws Exception {
        String[] termSelectionSet = { ",", ")" };
        if (NT().identifier == TokenIdentifier.TokenIdentifierTypeConstant) {
            NoteCenter.verbose("SB() identified a constant or identifier");
            if (SB()) {
                NoteCenter.verbose("Successfully returned from SB() to SB1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in SB1()->SB(); returning false");
                return false;
            }
        }
        else if (NT().identifier == TokenIdentifier.TokenIdentifierTypeIdentifier) {
            NoteCenter.verbose("SB() identified a constant or identifier");
            if (SB()) {
                NoteCenter.verbose("Successfully returned from SB() to SB1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in SB1()->SB(); returning false");
                return false;
            }
        }
        else if (NT().contents.equals(",")) {
            GT();
            NoteCenter.verbose("Successfully identified and consumed ',' in SB()");
            if (SB1()) {
                NoteCenter.verbose("Successfully returned to SB() from SB1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in SB()->SB1(); returning false");
                return false;
            }
        }
        else if (isInSet(NT(), termSelectionSet)) {
            NoteCenter.verbose("SB() found item in selection set; returning true");
            return true;
        }
        else {
            NoteCenter.error("Expected either ',', ')', identifier or constant but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // SB' -> cons SB | id SB
    public boolean SB1() throws Exception {
        if (NT().identifier == TokenIdentifier.TokenIdentifierTypeConstant) {
            NoteCenter.verbose("SB1() identified a constant or identifier");
            if (SB()) {
                NoteCenter.verbose("Successfully returned from SB() to SB1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in SB1()->SB(); returning false");
                return false;
            }
        }
        else if (NT().identifier == TokenIdentifier.TokenIdentifierTypeIdentifier) {
            NoteCenter.verbose("SB1() identified a constant or identifier");
            if (SB()) {
                NoteCenter.verbose("Successfully returned from SB() to SB1(); returning true");
                return true;
            }
            else {
                NoteCenter.verbose("Problem detected in SB1()->SB(); returning false");
                return false;
            }
        }
        else {
            NoteCenter.error("Expected an identifier or constant but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // S -> do S S' | (IL) S'' | assign E to ID; S | EPSILON        { do ( assign end unless when ; }
    public boolean S() throws Exception {
        String[] termSelectionSet = { "do", "(", "assign", "end.", "unless", "when", ";" };
        System.out.println("-- Here in S()");
        if (NT().contents.equals("do")) {
            GT();
            NoteCenter.verbose("S() successfully identified and consumed 'do'");
            if (S()) {
                NoteCenter.verbose("Successfully returned from S() to S()");
                if (S1()) {
                    NoteCenter.verbose("Successfully returned from S1() to S()");
                    return true;
                } else {
                    NoteCenter.verbose("S()->S1() detected problem; returning false");
                    return false;
                }
            }
            else {
                NoteCenter.verbose("S() detected an error; returning false");
                return false;
            }
        }
        // (IL) S''
        else if (NT().contents.equals("(")) {
            System.out.println("Here here --");
            GT();
            NoteCenter.verbose("S() successfully identified and consumed '('");
            if (IL()) {
                NoteCenter.verbose("IL() returned to S() successfully");
                if (NT().contents.equals(")")) {
                    GT();
                    NoteCenter.verbose("S() successfully identified and consumed ')'");
                    if (S2()) {
                        NoteCenter.verbose("S2() returned successfully to S(); returning true");
                        return true;
                    }
                    else {
                        NoteCenter.verbose("Detected error in S()->S2(); returning false");
                        return false;
                    }
                }
                else {
                    NoteCenter.error("Expected closing ')'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                NoteCenter.verbose("Detected problem in S()->IL(); returning false");
                return false;
            }
        }
        //assign E to ID; S
        else if (NT().contents.equals("assign")) {
            System.out.println("Now in assign");
            GT();
            NoteCenter.verbose("Detected and consumed 'assign'");
            if (E()) {
                NoteCenter.verbose("Successfully returned from E() to S()");
                if (NT().contents.equals("to")) {
                    GT();
                    NoteCenter.verbose("Detected and consumed 'to'");
                    if (ID()) {
                        NoteCenter.verbose("Successfully returned from ID() to S()");
                        if (NT().contents.equals(";")) {
                            GT();
                            NoteCenter.verbose("Detected and consumed ';'");
                            if (S()) {
                                NoteCenter.verbose("Successfully returned from S() to S(); returning true");
                                return true;
                            }
                            else {
                                NoteCenter.verbose("S() to S() determined a problem; returning false");
                                return false;
                            }
                        }
                        else {
                            NoteCenter.error("Expected ';' but got '"+NT().contents+"'", fileTokenizer.getLine());
                            return false;
                        }
                    }
                    else {
                        NoteCenter.verbose("Detected error from S() to ID(); returning false");
                        return false;
                    }
                }
                else {
                    NoteCenter.error("Expected 'to' but got '"+NT().contents+"'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                NoteCenter.verbose("Problem detected in E(); returning false");
                return false;
            }
        }
        else if (isInSet(NT(), termSelectionSet)) {
            
            NoteCenter.verbose("S() identified item in selection set; returning true");
            return true;
        }
        else {
             NoteCenter.error("Expected \"do\", \"(\", \"assign\", \"end.\", \"unless\", \"when\", \";\" but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // S' -> unless C; S | when C S'''
    public boolean S1() throws Exception {

        if (NT().contents.equals("unless")) {
            GT();
            NoteCenter.verbose("Found and consumed unless");
            if (C()) {
                if (NT().contents.equals(";")) {
                    GT();
                    if (S()) {
                        NoteCenter.verbose("S() passed.");
                        return true;
                    }
                    else {
                        NoteCenter.verbose("S() failed; returning false");
                        return false;
                    }
                }
                else {
                    NoteCenter.error("Expected ';' but got '"+NT().contents+"'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                NoteCenter.verbose("C() failed; returning false");
                return false;
            }
        }
        else if (NT().contents.equals("when")) {
            GT();
            NoteCenter.verbose("Found and consumed when");
            if (C()) {
                if (S3()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            NoteCenter.error("Expected 'unless' or 'when' but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }

    }

    // S'' -> in; S | out; S
    public boolean S2() throws Exception {
        if (NT().contents.equals("in")) {
            GT();
            if (NT().contents.equals(";")) {
                GT();
                if (S()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                NoteCenter.error("Expected ';' but got '"+NT().contents+"'", fileTokenizer.getLine());
                return false;
            }
        }
        else if (NT().contents.equals("out")) {
            GT();
            if (NT().contents.equals(";")) {
                GT();
                if (S()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                NoteCenter.error("Expected ';' but got '"+NT().contents+"'", fileTokenizer.getLine());
                return false;
            }
        }
        else {
            NoteCenter.error("Expected 'in' or 'out' but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // S''' -> ; S | else S; S
    public boolean S3() throws Exception {
        if (NT().contents.equals(";")) {
            GT();
            if (S()) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("else")) {
            GT();
            if (S()) {
                if (NT().contents.equals(";")) {
                    if (S()) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    NoteCenter.error("Expected ';' but got '"+NT().contents+"'", fileTokenizer.getLine());
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            NoteCenter.error("Expected ';' or 'else' but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // E -> id | cons | + E E | - E E | * E E | / E E
    public boolean E() throws Exception {
        if (NT().contents.equals("+")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("-")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("*")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("/")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().identifier == TokenIdentifier.TokenIdentifierTypeIdentifier) {
            GT();
            return true;
        }
        else if (NT().identifier == TokenIdentifier.TokenIdentifierTypeConstant) {
            GT();
            return true;
        }

        else {
            NoteCenter.error("Expected '+', '-', '*', '/', IDENTIFIER, or CONSTANT but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

    // C -> < E E | > E E | = E E | and C C | not C C
    public boolean C() throws Exception {
        if (NT().contents.equals("<")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals(">")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("=")) {
            GT();
            if (E()) {
                if (E()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("and")) {
            GT();
            if (C()) {
                if (C()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (NT().contents.equals("not")) {
            GT();
            if (C()) {
                if (C()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            NoteCenter.error("Expected '<', '>', '=', 'and', or 'not' but got '"+NT().contents+"'", fileTokenizer.getLine());
            return false;
        }
    }

// ---

    private boolean isInSet(Token token, String[] set) {
        String contents = token.contents;
        for (int i = 0; i < set.length; i++) {
            if (contents.equals(set[i])) {
                NoteCenter.verbose("Found match in selection set with Token="+contents+" and '"+set[i]+"'");
                return true;
            }
        }
        return false;
    }

    private boolean typeIsInSet(Token token, TokenIdentifier identifier) {
        if (token.identifier == identifier) {
            NoteCenter.verbose("Found matching identifier in selection set with Token identifier="+token.identifier.name()+" and '"+identifier.name()+"'");
            return true;
        }
        return false;
    }
}
