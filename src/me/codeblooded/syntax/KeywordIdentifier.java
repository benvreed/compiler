package me.codeblooded.syntax;

import me.codeblooded.errors.SyntaxAnalyzerException;
import me.codeblooded.lexical.Token;
import me.codeblooded.lexical.TokenIdentifier;
import java.util.regex.*;

/**
 * Created by ben on 9/27/14.
 */
public class KeywordIdentifier {

    /**
     * A token which is used to globally match against.
     */
    private Token globalToken;

    /**
     * Identifies a token and sets its identity.
     * @param token The token.
     */
    public void identify(Token token) {
        // set the token that the identification methods rely on
        globalToken = token;
        if (!identifiedKeywords()) {
            // if keywords could not be found, try to identify constants
            if (!identifiedConstants()) {
                // if constants could not be found, try to identify identifiers
                if (!identifiedIds()) {
                    // if identifiers could not be found, throw an error
                    System.out.println("(COMPILER)-> ERROR: UNRECOGNIZED TOKEN '"+globalToken.contents+"'.");
                }
            }
        }
    }

    private boolean identifiedKeywords() {
        // TODO: Implement Keyword Identification
        String[] keywords = {"program", "begin", "end", ".", "array", "integer", "(", ")", ",", "do", "unless", "when", ";", ":", "else", "in", "out", "assign", "to", "+", "-", "*", "/", "!", "?", "=", "and", "or", "not"};

        for (int i=0; i < keywords.length; i++) {
            if (keywords[i].equals(globalToken.contents)) {
                globalToken.identifier = TokenIdentifier.TokenIdentifierTypeKeyword;
                return true;
            }
        }
        return false;
    }

    private boolean identifiedConstants() {
        String pattern = "[0-9]+(.)*([0-9])*";
        // match the token's contents and the pattern
        if (globalToken.contents.matches(pattern)) {
            globalToken.identifier = TokenIdentifier.TokenIdentifierTypeConstant;
            return true;
        }
        return false;
    }

    private boolean identifiedIds() {
        String pattern = "[A-Za-z]+[A-Za-z0-9]*";
        // TODO: ADD RESTRICTIONS ON KEYWORDS IF NECESSARY
        if (globalToken.contents.matches(pattern))
        {
            globalToken.identifier = TokenIdentifier.TokenIdentifierTypeIdentifier;
        }
        //globalToken.identifier = TokenIdentifier.TokenIdentifierTypeIdentifier;
        return true;
    }

}
